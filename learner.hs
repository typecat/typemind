{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Learner where

import Prelude hiding (id, (.))
import Control.Category

data Learner p a b = Learner
  { _implement :: p -> a -> b
  , _update :: b -> p -> a -> p
  , _request :: b -> p -> a -> a
  }

instance Category (Learner p) where
  id :: Learner p a a
  id = identityLearner
  (.) :: Learner p b c -> Learner p a b -> Learner p a c
  (.) = badCompose

badCompose :: forall p a b c. Learner p b c -> Learner p a b -> Learner p a c
badCompose = undefined

compose :: forall p q a b c. Learner q b c -> Learner p a b -> Learner (p, q) a c
compose l1 l2 =
  let i :: p -> a -> b
      i = _implement l2
      u :: b -> p -> a -> p
      u = _update l2
      r :: b -> p -> a -> a
      r = _request l2
      j :: q -> b -> c
      j = _implement l1
      v :: c -> q -> b -> q
      v = _update l1
      s :: c -> q -> b -> b
      s = _request l1
      -- (I∗J)(q,p,a) := J(q,I(p,a))
      ij :: (p, q) -> a -> c
      ij (p, q) a = j q (i p a)
      -- u': (U‖V)(p,q,a,c,b,d) := (U(p,a,s(q,I(p,a),c)),V(q,I(p,a),c))
      u' :: c -> (p, q) -> a -> (p, q)
      u' c (p, q) a = (u (s c q (i p a)) p a, v c q (i p a))
      -- r' (r∗s)(q,p,a,c) := r(p,a,s(q,I(p,a),c))
      r' :: c -> (p, q) -> a -> a
      r' c (p, q) a = r (s c q (i p a)) p a
  in Learner ij u' r'

identityLearner :: forall p a. Learner p a a
identityLearner = Learner i u r
  where i :: p -> a -> a
        i p a = a
        u :: b -> p -> a -> p
        u b p a = p
        r :: a -> p -> a -> a
        r b p a = a


-----------------
---- Example ---- 
-----------------

type Weight = [Double]
type Point  =  [Double]
type Label = Double
type Datum = (Point,Label)
type Data  = [Datum]

l1 :: Learner Weight Point Label
l1 = undefined

l2 :: Learner Weight Label Point
l2 = undefined

l3 :: Learner (Weight, Weight) Label Label
l3 = compose l1 l2
